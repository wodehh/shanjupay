package com.shanjupay.merchant.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.CommonErrorCode;
import com.shanjupay.common.domain.PageVO;
import com.shanjupay.common.util.PhoneUtil;
import com.shanjupay.common.util.StringUtil;
import com.shanjupay.merchant.MerchantService;
import com.shanjupay.merchant.convert.StaffConvert;
import com.shanjupay.merchant.convert.StoreConvert;
import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.convert.MerchantConvert;
import com.shanjupay.merchant.dto.StaffDTO;
import com.shanjupay.merchant.dto.StoreDTO;
import com.shanjupay.merchant.entity.Merchant;
import com.shanjupay.merchant.entity.Staff;
import com.shanjupay.merchant.entity.Store;
import com.shanjupay.merchant.entity.StoreStaff;
import com.shanjupay.merchant.mapper.MerchantMapper;
import com.shanjupay.merchant.mapper.StaffMapper;
import com.shanjupay.merchant.mapper.StoreMapper;
import com.shanjupay.merchant.mapper.StoreStaffMapper;
import com.shanjupay.user.api.TenantService;
import com.shanjupay.user.api.dto.tenant.CreateTenantRequestDTO;
import com.shanjupay.user.api.dto.tenant.TenantDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator.
 */
@org.apache.dubbo.config.annotation.Service
@Transactional
@Slf4j
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    MerchantMapper merchantMapper;

    @Autowired
    StoreMapper storeMapper;

    @Autowired
    StaffMapper staffMapper;

    @Autowired
    StoreStaffMapper storeStaffMapper;

    @Reference
    TenantService tenantService;

    /**
     * 根据ID查询用户信息
     *
     * @param id
     * @return
     */
    @Override
    public MerchantDTO queryMerchantById(Long id) {
        Merchant merchant = merchantMapper.selectById(id);
        // MerchantDTO merchantDTO = new MerchantDTO();
        // merchantDTO.setId(merchant.getId());
        // merchantDTO.setMerchantName(merchant.getMerchantName());
        return MerchantConvert.INSTANCE.entity2dto(merchant);
    }


    /**
     * 注册商户服务接口，接收账号、密码、手机号，为了可拓展性使用merchantDto接收数据
     * 调用SaaS接口：新增租户、用户、绑定租户和用户关系，初始化权限
     *
     * @param merchantDTO 注册的商户信息
     * @return 注册成功的商户信息
     * @throws BusinessException
     */
    @Override
    public MerchantDTO createMerchant(MerchantDTO merchantDTO) throws BusinessException {

        // 校验参数合法性
        if (merchantDTO == null) {
            throw new BusinessException(CommonErrorCode.E_100108);
        }
        // 校验手机号是否为空
        if (StringUtil.isBlank(merchantDTO.getMobile())) {
            throw new BusinessException(CommonErrorCode.E_100112);
        }
        // 校验手机号格式不正确
        if (PhoneUtil.isMatches(merchantDTO.getMobile())) {
            throw new BusinessException(CommonErrorCode.E_100109);
        }
        // 密码不能为空
        if (StringUtil.isBlank(merchantDTO.getPassword())) {
            throw new BusinessException(CommonErrorCode.E_100111);
        }

        // 检验手机号的唯一性
        // 根据手机号查询商户表，如果存在记录则说明手机号已经存在
        Integer count = merchantMapper.selectCount(new LambdaQueryWrapper<Merchant>().eq(Merchant::getMobile, merchantDTO.getMobile()));
        if (count > 0) {
            throw new BusinessException(CommonErrorCode.E_100113);
        }

        // 调用SaaS接口
        CreateTenantRequestDTO createTenantRequestDTO = new CreateTenantRequestDTO();
        createTenantRequestDTO.setMobile(merchantDTO.getMobile());
        createTenantRequestDTO.setUsername(merchantDTO.getUsername());
        createTenantRequestDTO.setPassword(merchantDTO.getPassword());

        // 租户类型
        createTenantRequestDTO.setTenantTypeCode("shanju-merchant");
        // 套餐根据套餐进行权限分配
        createTenantRequestDTO.setBundleCode("shanju-merchant");
        // 租户名称，和账号名称一样
        createTenantRequestDTO.setName(merchantDTO.getUsername());
        // 如果租户在SaaS已经存在，SaaS直接返回租户的信息，否则进行添加
        TenantDTO tenantAndAccount = tenantService.createTenantAndAccount(createTenantRequestDTO);
        // 获取租户的ID
        if (tenantAndAccount.getId() == null || tenantAndAccount == null) {
            throw new BusinessException(CommonErrorCode.E_200012);
        }
        // 租户的ID
        Long tenantId = tenantAndAccount.getId();

        // 租户ID在商户表唯一
        // 根据租户ID从商户表查询，如果存在记录则不允许添加商户
        Integer count1 = merchantMapper.selectCount(new LambdaQueryWrapper<Merchant>().eq(Merchant::getTenantId, tenantId));
        if (count1 > 0) {
            throw new BusinessException(CommonErrorCode.E_200017);
        }
        // Merchant merchant = new Merchant();
        // merchant.setMobile(merchantDTO.getMobile());

        // 使用mapstruct来进行数据映射转换
        Merchant merchant = MerchantConvert.INSTANCE.dto2entity(merchantDTO);
        // 设置所对应的租户的ID
        merchant.setTenantId(tenantId);
        // 审核状态为0-未进行资质申请
        merchant.setAuditStatus("0");
        // 调用mapper向数据库写入
        merchantMapper.insert(merchant);

        // 新增门店
        StoreDTO storeDTO = new StoreDTO();
        storeDTO.setStoreName("根门店");
        // 商户ID
        storeDTO.setMerchantId(merchant.getId());
        // storeDTO.setStoreStatus(true);
        StoreDTO store = createStore(storeDTO);

        // 新增员工
        StaffDTO staffDTO = new StaffDTO();
        // 手机号
        staffDTO.setMobile(merchantDTO.getMobile());
        // 帐号
        staffDTO.setUsername(merchantDTO.getUsername());
        // 员工所属门店ID
        staffDTO.setStoreId(store.getId());
        // 商户ID
        staffDTO.setMerchantId(merchant.getId());
        // 员工状态为启用
        // staffDTO.setStaffStatus(true);
        StaffDTO staff = createStaff(staffDTO);

        // 为门店设置管理员
        bindStaffToStore(store.getId(), staff.getId());


        // 将dto中写入新增商户的ID
        // merchantDTO.setId(merchant.getId());

        // entity转换成dto
        return MerchantConvert.INSTANCE.entity2dto(merchant);
    }

    /**
     * 资质申请的接口
     *
     * @param merchantId  商户id
     * @param merchantDTO 资质申请的信息
     * @throws BusinessException
     */
    @Override
    public void applyMerchant(Long merchantId, MerchantDTO merchantDTO) throws BusinessException {
        if (merchantId == null || merchantDTO == null) {
            throw new BusinessException(CommonErrorCode.E_300009);
        }
        // 检验merchantId合法性,查询商户表,如果查询不到记录,认为非法
        Merchant merchant = merchantMapper.selectById(merchantId);
        if (merchant == null) {
            throw new BusinessException(CommonErrorCode.E_300002);
        }
        // 将Dto转为entity
        Merchant entity = MerchantConvert.INSTANCE.dto2entity(merchantDTO);
        // 将必要的参数设置到entity,其他字段进行更新
        entity.setId(merchant.getId());
        // 因为资质申请的时候手机号不让改，还使用数据库中原来的手机号
        entity.setMobile(merchant.getMobile());
        // 审核状态1-已申请待审核
        entity.setAuditStatus("1");
        entity.setTenantId(merchant.getTenantId());
        // 调用mapper更新商户表
        merchantMapper.updateById(entity);
    }

    /**
     * 新增门店
     *
     * @param storeDTO 门店信息
     * @return 新增成功的门店信息
     * @throws BusinessException
     */
    @Override
    public StoreDTO createStore(StoreDTO storeDTO) throws BusinessException {
        Store entity = StoreConvert.INSTANCE.dto2entity(storeDTO);
        log.info("新增门店{}", JSON.toJSONString(entity));
        storeMapper.insert(entity);
        return StoreConvert.INSTANCE.entity2dto(entity);
    }

    /**
     * 新增员工
     *
     * @param staffDTO 员工信息
     * @return 新增员工信息
     * @throws BusinessException
     */
    @Override
    public StaffDTO createStaff(StaffDTO staffDTO) throws BusinessException {
        // 参数合法性校验
        if (staffDTO == null || StringUtil.isBlank(staffDTO.getMobile())
                || StringUtil.isBlank(staffDTO.getUsername()) ||
                staffDTO.getStoreId() == null) {
            throw new BusinessException(CommonErrorCode.E_300009);
        }
        // 在同一个商户下员工的账号唯一
        Boolean existStaffByUserName = isExistStaffByUserName(staffDTO.getUsername(), staffDTO.getMerchantId());
        if (existStaffByUserName) {
            throw new BusinessException(CommonErrorCode.E_100114);
        }
        // 在同一个商户下员工的手机号唯一
        Boolean existStaffByMobile = isExistStaffByMobile(staffDTO.getMobile(), staffDTO.getMerchantId());
        if (existStaffByMobile) {
            throw new BusinessException(CommonErrorCode.E_100113);
        }
        Staff entity = StaffConvert.INSTANCE.dto2entity(staffDTO);
        staffMapper.insert(entity);
        return StaffConvert.INSTANCE.entity2dto(entity);
    }

    /**
     * 将员工设置为门店的管理员
     *
     * @param storeId
     * @param staffId
     * @throws BusinessException
     */
    @Override
    public void bindStaffToStore(Long storeId, Long staffId) throws BusinessException {
        StoreStaff storeStaff = new StoreStaff();
        storeStaff.setStaffId(staffId);
        storeStaff.setStoreId(storeId);
        storeStaffMapper.insert(storeStaff);
    }

    /**
     * 根据租户iD查询商户信息
     *
     * @param tenantId
     * @return
     */
    @Override
    public MerchantDTO queryMerchantByTenantId(Long tenantId) {
        Merchant merchant = merchantMapper.selectOne(new LambdaQueryWrapper<Merchant>().eq(Merchant::getTenantId, tenantId));
        return MerchantConvert.INSTANCE.entity2dto(merchant);
    }

    /**
     * 分页条件查询商户下门店
     *
     * @param storeDTO 查询条件，必要参数：商户ID
     * @param pageNo   页码
     * @param pageSize 分页记录数
     * @return
     */
    @Override
    public PageVO<StoreDTO> queryStoreByPage(StoreDTO storeDTO, Integer pageNo, Integer pageSize) {
        // 分页条件
        Page<Store> page = new Page<>(pageNo, pageSize);
        // 查询条件拼装
        LambdaQueryWrapper<Store> storeDTOLambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 如果 传入商户id此时要拼装查询条件
        if (storeDTO != null && storeDTO.getMerchantId() != null) {
            storeDTOLambdaQueryWrapper.eq(Store::getMerchantId, storeDTO.getMerchantId());
        }
        if (storeDTO != null && StringUtil.isEmpty(storeDTO.getStoreName())) {
            storeDTOLambdaQueryWrapper.eq(Store::getStoreName, storeDTO.getStoreName());
        }
        // 查询数据库
        IPage<Store> storeIPage = storeMapper.selectPage(page, storeDTOLambdaQueryWrapper);
        // 查询列表
        List<Store> records = storeIPage.getRecords();
        List<StoreDTO> storeDTOS = StoreConvert.INSTANCE.listentity2dto(records);

        return new PageVO(storeDTOS, storeIPage.getTotal(), pageNo, pageSize);
    }

    /**
     * 校验门店是否属于商户
     *
     * @param storeId
     * @param merchantId
     * @return true表示存在，false表示不存在
     */
    @Override
    public Boolean queryStoreInMerchant(Long storeId, Long merchantId) {
        Integer count = storeMapper.selectCount(new LambdaQueryWrapper<Store>().eq(Store::getId,storeId)
                                                .eq(Store::getMerchantId,merchantId));
        return count > 0;
    }

    /**
     * 员工手机号在同一个商户下是唯一校验
     *
     * @param mobile
     * @param merchant
     * @return
     */
    Boolean isExistStaffByMobile(String mobile, Long merchant) {
        Integer count = staffMapper.selectCount(new LambdaQueryWrapper<Staff>()
                .eq(Staff::getMobile, mobile)
                .eq(Staff::getMerchantId, merchant));
        return count > 0;
    }

    /**
     * 员工账号在同一个商户下是唯一校验
     *
     * @param username
     * @param merchantId
     * @return
     */
    Boolean isExistStaffByUserName(String username, Long merchantId) {
        Integer count = staffMapper.selectCount(new LambdaQueryWrapper<Staff>()
                .eq(Staff::getUsername, username)
                .eq(Staff::getMerchantId, merchantId));
        return count > 0;
    }
}
