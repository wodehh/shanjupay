package com.shanjupay.merchant.convert;

import com.shanjupay.merchant.dto.AppDTO;
import com.shanjupay.merchant.entity.App;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author ThinkD
 * @Des TODO
 * @date 2021-07-01 14:14
 */
@Mapper
public interface AppConvert {

    AppConvert INSTANCE = Mappers.getMapper(AppConvert.class);

    AppDTO entity2dto(App entity);

    App dto2entity(AppDTO appDTO);

    List<AppDTO> listEntity2dto(List<App> app);

}
