package com.shanjupay.merchant.convert;

import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.entity.Merchant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author ThinkD
 * @Des 使用MapStruct, 对Entity和DTO之间的映射转换规则
 * @date 2021-06-28 14:37
 */

// 对象属性的映射
@Mapper
public interface MerchantConvert {

    // 转换类型实例
    MerchantConvert INSTANCE = Mappers.getMapper(MerchantConvert.class);

    // 将dto转换成entity
    Merchant dto2entity(MerchantDTO merchantDTO);

    // 将entity转换成dto
    MerchantDTO entity2dto(Merchant merchant);

    // 将list中有entity转换为list中有dto的
    List<MerchantDTO> listEntity2ListDto(List<Merchant> merchants);


    // 直接接口测试
    public static void main(String[] args) {

        // 首先创建一个entity对象实例
        Merchant merchant = new Merchant();
        merchant.setMerchantName("12345");
        merchant.setMerchantNo(1212L);

        // 转换entity实例对象为Dto对象属性
        MerchantDTO merchantDTO = MerchantConvert.INSTANCE.entity2dto(merchant);
        System.out.println(merchantDTO);

        // 创建一个Dto对象
        MerchantDTO merchantDTO1 = new MerchantDTO();
        merchantDTO1.setUsername("Ace");

        // 转换Dto对象属性为entity实例对象
        Merchant merchant1 = MerchantConvert.INSTANCE.dto2entity(merchantDTO1);
        System.out.println(merchant1);

        // 创建一个list
        List entityList = new ArrayList();
        entityList.add(merchant);

        List list = MerchantConvert.INSTANCE.listEntity2ListDto(entityList);
        System.out.println(list);

    }
}
