package com.shanjupay.merchant;

import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.PageVO;
import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.dto.StaffDTO;
import com.shanjupay.merchant.dto.StoreDTO;
import com.shanjupay.merchant.dto.StoreStaffDTO;

/**
 * Created by Administrator.
 */
public interface MerchantService {

    /**
     * 根据 id查询商户
     *
     * @param id
     * @return
     */
    MerchantDTO queryMerchantById(Long id);


    /**
     * 注册商户接口，接收姓名、密码、手机号，为了可扩展性使用DTO接收数据
     *
     * @param merchantDTO 注册的商户信息
     * @return 返回注册成功的商户信息
     * @throws BusinessException
     */
    MerchantDTO createMerchant(MerchantDTO merchantDTO) throws BusinessException;


    /**
     * 资质申请的接口
     *
     * @param merchantId  商户id
     * @param merchantDTO 资质申请的信息
     * @throws BusinessException
     */
    void applyMerchant(Long merchantId, MerchantDTO merchantDTO) throws BusinessException;


    /**
     * 新增门店
     *
     * @param storeDTO 门店信息
     * @return 新增成功的门店信息
     * @throws BusinessException
     */
    StoreDTO createStore(StoreDTO storeDTO) throws BusinessException;


    /**
     * 新增员工
     *
     * @param staffDTO 员工信息
     * @return 新增员工信息
     * @throws BusinessException
     */
    StaffDTO createStaff(StaffDTO staffDTO) throws BusinessException;


    /**
     * 将员工设置为门店的管理员
     *
     * @param storeId
     * @param staffId
     * @throws BusinessException
     */
    void bindStaffToStore(Long storeId, Long staffId) throws BusinessException;

    /**
     * 根据租户iD查询商户信息
     *
     * @param tenantId
     * @return
     */
    MerchantDTO queryMerchantByTenantId(Long tenantId);


    /**
     * 分页条件查询商户下门店
     *
     * @param storeDTO 查询条件，必要参数：商户ID
     * @param pageNo   页码
     * @param pageSize 分页记录数
     * @return
     */
    PageVO<StoreDTO> queryStoreByPage(StoreDTO storeDTO, Integer pageNo, Integer pageSize);


    /**
     * 校验门店是否属于商户
     * @param StoreId
     * @param merchantId
     * @return
     */
    Boolean queryStoreInMerchant(Long StoreId, Long merchantId);
}
