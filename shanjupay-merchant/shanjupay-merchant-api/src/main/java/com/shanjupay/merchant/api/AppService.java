package com.shanjupay.merchant.api;

import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.merchant.dto.AppDTO;


import java.util.List;

/**
 * @Author ThinkD
 * @Des 应用管理相关接口
 * @date 2021-07-01 14:06
 */
public interface AppService {

    /**
     * 创建应用
     *
     * @param merchantId 商户ID
     * @param appDTO     应用信息
     * @return 创建成功的应用的信息
     * @throws BusinessException
     */
    AppDTO createApp(Long merchantId, AppDTO appDTO) throws BusinessException;

    /**
     * 根据商户ID查询应用列表
     * @param merchantId
     * @return
     * @throws BusinessException
     */
    List<AppDTO> queryAppByMerchant(Long merchantId) throws BusinessException;


    /**
     * 根据应用的ID查询应用的信息
     * @param appId
     * @return
     * @throws BusinessException
     */
    AppDTO getAppById(String appId) throws BusinessException;


    /**
     * 查询应用是否属于商户
     * @param appid
     * @param merchantID
     * @return
     * @throws BusinessException
     */
    Boolean queryAppInMerchant(String appid,Long merchantID) throws BusinessException;
}
