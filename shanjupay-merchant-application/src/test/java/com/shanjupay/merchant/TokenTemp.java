package com.shanjupay.merchant;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.shanjupay.common.util.EncryptUtil;
import com.shanjupay.merchant.dto.MerchantDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author ThinkD
 * @Des 生成令牌的工具类
 * @date 2021-06-30 17:17
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TokenTemp {

    @Autowired
    MerchantService merchantService;


    @Test
    public void createTestToken(){
        // 填写用于测试的商户ID
        Long merchantId = 1209826678635798530L;
        MerchantDTO merchantDTO = new MerchantDTO();
        JSONObject token = new JSONObject();

        token.put("mobile",merchantDTO.getMobile());
        token.put("user_name",merchantDTO.getMerchantName());
        token.put("merchantId",merchantId);

        String jwt_token = "Bearer "+ EncryptUtil.encodeBase64(JSON.toJSONString(token).getBytes());
        System.out.println(jwt_token);
    }
}
