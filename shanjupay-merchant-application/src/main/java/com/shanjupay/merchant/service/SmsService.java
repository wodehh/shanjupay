package com.shanjupay.merchant.service;

import com.shanjupay.common.domain.BusinessException;

/**
 * @Author ThinkD
 * @Description TODO
 * @date 2021-06-25 9:53
 */
public interface SmsService {

    /**
     * Des:发送手机验证码的接口
     * @param phone
     * @return 验证码对应的key
     */
    String sendMsg(String phone);


    /**
     * Des:检查手机的验证码是否正确
     * @param verifyKey    验证码的Key
     * @param verifyCode   验证码的Code
     */
    // 声明抛出自定义异常
    void checkVerifyCode(String verifyKey,String verifyCode) throws BusinessException;
}
