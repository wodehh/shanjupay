package com.shanjupay.merchant.service;

import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.CommonErrorCode;
import com.shanjupay.common.util.QiniuUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;


/**
 * @Author ThinkD
 * @Des TODO
 * @date 2021-06-29 15:51
 */
@Service  // 实例为一个bean
public class FileServiceImpl implements FileService {

    // 七牛云nacos中心配置信息
    @Value("${oss.qiniu.url}")
    private String qiniuUrl;

    @Value("${oss.qiniu.accessKey}")
    private String accessKey;

    @Value("${oss.qiniu.secretKey}")
    private String secretKey;

    @Value("${oss.qiniu.bucket}")
    private String bucket;

    /**
     * @param bytes    文件字节数组
     * @param fileName 文件名称
     * @return 文件下载路径(绝对的URL下载路径)
     * @throws BusinessException
     */
    @Override
    public String upLoad(byte[] bytes, String fileName) throws BusinessException {
        // 调用common下的工具类
        // String accessKey,String secretKey,String bucket,byte[] bytes,String fileName
        try {
            QiniuUtils.upload2qiniu(accessKey, secretKey, bucket, bytes, fileName);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new BusinessException(CommonErrorCode.E_100106);
        }
        // 上传成功返回文件的绝对访问(绝对路径)
        return qiniuUrl + fileName;
    }
}
