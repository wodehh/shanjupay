package com.shanjupay.merchant.service;

import com.alibaba.fastjson.JSON;
import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.CommonErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author ThinkD
 * @Description TODO
 * @date 2021-06-25 10:02
 */


@Service // 实例为一个bean
@Slf4j
public class SmsServiceImpl implements SmsService {

    @Value("${sms.url}")
    String url;

    @Value("${sms.effectiveTime}")
    String effectiveTime;

    @Autowired
    RestTemplate restTemplate;


    /**
     * Des:发送手机验证码的接口
     *
     * @param phone
     * @return 验证码对应的key
     */
    @Override
    public String sendMsg(String phone) {

        // 发送验证码的url
        String sms_url = url + "/generate?name=sms&effectiveTime=" + effectiveTime;
        // 请求体
        Map<String, Object> body = new HashMap<>();
        body.put("mobile", phone);
        // 请求头
        HttpHeaders httpHeaders = new HttpHeaders();
        // content-type: application/json;charset=UTF-8
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        // 请求信息，传入body,header
        HttpEntity httpEntity = new HttpEntity(body, httpHeaders);
        // 将得到的bodyMap提取为公共变量
        Map bodyMap;

        // 向url请求
        try {
            // 使用restTemplate发起请求
            ResponseEntity<Map> exchange = restTemplate.exchange
                    (sms_url, HttpMethod.POST, httpEntity, Map.class);
            // 将响应打印在控制台
            log.info("请求验证码服务，得到响应：{}", JSON.toJSONString(exchange));
            bodyMap = exchange.getBody();
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            throw new RuntimeException("发送验证码失败");
        }
        if (bodyMap == null || bodyMap.get("result") == null) {
            throw new RuntimeException("发送验证码失败");
        }

        Map result = (Map) bodyMap.get("result");
        return result.get("key").toString();
    }

    /**
     * Des:检查手机的验证码是否正确
     *
     * @param verifyKey  验证码的Key
     * @param verifyCode 验证码的Code
     */
    @Override
    public void checkVerifyCode(String verifyKey, String verifyCode) throws BusinessException {
        // 校验验证码的url
        String check_url = url + "/verify?name=sms&verificationCode=" + verifyCode + "&verificationKey=" + verifyKey;

        Map bodyMap;
        try {
            ResponseEntity<Map> exchange =
                    restTemplate.exchange(check_url, HttpMethod.POST, HttpEntity.EMPTY, Map.class);
            // 将响应打印在控制台
            log.info("请求验证码服务，得到响应：{}", JSON.toJSONString(exchange));
            bodyMap = exchange.getBody();
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            throw new BusinessException(CommonErrorCode.E_100102);
            // throw new RuntimeException("校验验证码失败");
        }
        if (bodyMap == null || bodyMap.get("result") == null || !(Boolean) bodyMap.get("result")) {
            throw new BusinessException(CommonErrorCode.E_100102);
        }
    }
}
