package com.shanjupay.merchant.service;

import com.shanjupay.common.domain.BusinessException;
import org.springframework.stereotype.Service;

/**
 * @Author ThinkD
 * @Des TODO
 * @date 2021-06-29 15:43
 */
public interface FileService {


    /**
     *
     * @param bytes   文件字节数组
     * @param fileName  文件名称
     * @return  文件下载路径(绝对的URL下载路径)
     * @throws BusinessException
     */

    String upLoad(byte[] bytes,String fileName) throws BusinessException;

}
