package com.shanjupay.merchant.controller;

import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.CommonErrorCode;
import com.shanjupay.merchant.common.util.SecurityUtil;
import com.shanjupay.transaction.api.PayChannelService;
import com.shanjupay.transaction.api.dto.PayChannelDTO;
import com.shanjupay.transaction.api.dto.PayChannelParamDTO;
import com.shanjupay.transaction.api.dto.PlatformChannelDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author ThinkD
 * @Des 平台支付相关的配置controller
 * @date 2021-07-03 13:38
 */
@RestController
@Slf4j
@Api(value = "商户平台-渠道和支付参数相关", tags = "商户平台-渠道和支付参数", description = "商户平台-渠道和支付参数相关")
public class PlatformParamController {


    @Reference
    PayChannelService payChannelService;


    @ApiOperation("获取平台服务类型")
    @GetMapping("/my/platform-channels")
    public List<PlatformChannelDTO> queryPlatformChannel() {
        return payChannelService.queryPlatformChannel();
    }


    @ApiOperation("应用绑定一个服务类型")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "应用id", name = "appid", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(value = "服务类型Code", name = "platformChannelCode", required = true, dataType = "String", paramType = "query")
    })
    @PostMapping("/my/apps/{appid}/platform-channels")
    public void bindPlatformForApp(@PathVariable("appid") String appid,
                                   @RequestParam("platformChannelCode") String platformChannelCode) {
        payChannelService.bindPlatformChannelForApp(appid, platformChannelCode);
    }


    @ApiOperation("查询一个应用和服务类型的绑定状态")
    @GetMapping("/my/merchants/apps/platformChannels")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "应用id", name = "appid", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(value = "服务类型", name = "platformChannel", required = true, dataType = "String", paramType = "query")
    })
    public int queryAppBindPlatformChannel(@RequestParam("appid") String appid,
                                           @RequestParam("platformChannel") String platformChannel) {
        return payChannelService.queryAppBindPlatformChannel(appid, platformChannel);
    }


    @ApiOperation("根据平台服务类型获取支付渠道配置")
    @ApiImplicitParams(@ApiImplicitParam(name = "platformChannelCode", value = "服务类型编码", required = true, dataType = "String", paramType = "path"))
    @GetMapping(value = "/my/pay-channels/platform-channel/{platformChannelCode}")
    public List<PayChannelDTO> queryPayChannelByPlatformChannel(@PathVariable("platformChannelCode") String platformChannelCode) {
        return payChannelService.queryPayChannelByPlatformChannel(platformChannelCode);
    }


    @ApiOperation("商户配置支付渠道参数")
    @ApiImplicitParams(@ApiImplicitParam(name = "payChannelParam", value = "商户配置支付渠道参数", required = true, dataType = "PayChannelParamDTO", paramType = "body"))
    @RequestMapping(value = "/my/pay-channel-params", method = {RequestMethod.POST, RequestMethod.PUT})
    public void createPayChannelParam(@RequestBody PayChannelParamDTO payChannelParam) {
        if (payChannelParam == null || payChannelParam.getChannelName()==null){
            throw new BusinessException(CommonErrorCode.E_300009);
        }
        Long merchantId = SecurityUtil.getMerchantId();
        payChannelParam.setMerchantId(merchantId);
        payChannelService.savePayChannelParam(payChannelParam);
    }



    @ApiOperation("获取指定应用指定服务类型下所包含的原始支付渠道参数列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appId",value = "应用ID",required = true,dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "platformChannel",value = "服务类型",required = true,dataType = "String",paramType = "path")
    })
    @GetMapping(value = "/my/pay-channel-params/apps/{appId}/platform-channels/{platformChannel}")
    public List<PayChannelParamDTO> queryPayChannelParam(@PathVariable("appId") String appId,
                                                         @PathVariable("platformChannel") String platformChannel){
        return payChannelService.queryPayChannelParamByAppAndPlatform(appId,platformChannel);
    }



    @ApiOperation("获得指定应用指定服务类型下所包含的某个原始支付参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appId",value = "应用Id",required = true,dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "platformChannel",value = "服务类型代码",required = true,dataType = "String",paramType = "path"),
            @ApiImplicitParam(name = "payChannel",value = "支付渠道代码",required = true,dataType = "String",paramType = "path")
    })
    @GetMapping(value = "/my/pay-channel-params/apps/{appId}/platform-channels/{platformChannel}/platform-channels/{payChannel}")
    public PayChannelParamDTO queryPayChannelParam(@PathVariable("appId") String appId,
                                                   @PathVariable("platformChannel") String platformChannel,
                                                   @PathVariable("payChannel") String payChannel){
        return payChannelService.queryPayChannelParamByAppAndPlatformAndPayChannel(appId,platformChannel,payChannel);
    }
}
