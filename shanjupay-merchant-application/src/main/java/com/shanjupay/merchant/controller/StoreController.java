package com.shanjupay.merchant.controller;

import com.shanjupay.common.domain.PageVO;
import com.shanjupay.common.util.QRCodeUtil;
import com.shanjupay.merchant.MerchantService;
import com.shanjupay.merchant.common.util.SecurityUtil;
import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.dto.StoreDTO;
import com.shanjupay.transaction.api.TransactionService;
import com.shanjupay.transaction.api.dto.QRCodeDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @Author ThinkD
 * @Des TODO
 * @date 2021-07-17 14:18
 */
@RestController
@Slf4j
@Api(value = "商户平台-门店管理", tags = "商户平台-门店管理", description = "商户平台-增删改查")
public class StoreController {



    //"%s商品"
    @Value("${shanjupay.c2b.subject}")
    String subject;

    //"向%s付款"
    @Value("${shanjupay.c2b.body}")
    String body;

    @Reference
    MerchantService merchantService;

    @Reference
    TransactionService transactionService;


    @ApiOperation("分页查找商户下属门店")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int",paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int",paramType = "query")}
    )
    @PostMapping("/my/stores/merchants/page")
    public PageVO<StoreDTO> queryStoreByPage(@RequestParam Integer pageNo,@RequestParam Integer pageSize) {
        // 商户ID
        Long merchantId = SecurityUtil.getMerchantId();
        // 查询条件
        StoreDTO storeDTO = new StoreDTO();
        storeDTO.setMerchantId(merchantId);
        // 调用service返回查询结果
        return merchantService.queryStoreByPage(storeDTO,pageNo, pageSize);
    }


    @ApiOperation("生成商户应用门店的二维码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "appId", value = "商户应用id", required = true, dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "storeId", value = "商户门店id", required = true, dataType = "String", paramType = "path"),
    })
    @GetMapping(value = "/my/apps/{appId}/stores/{storeId}/app-store-qrcode")
    public String createCScanBStoreQRCode(@PathVariable("storeId") Long storeId, @PathVariable("appId")String appId) throws IOException {

        // 获取商户id
        Long merchantId = SecurityUtil.getMerchantId();
        //商户信息
        MerchantDTO merchantDTO = merchantService.queryMerchantById(merchantId);
        QRCodeDTO qrCodeDTO = new QRCodeDTO();
        qrCodeDTO.setMerchantId(merchantId);
        qrCodeDTO.setAppId(appId);
        //标题.用商户名称替换 %s
        String subjectFormat = String.format(subject, merchantDTO.getMerchantName());
        qrCodeDTO.setSubject(subjectFormat);
        // 内容
        String format1 = String.format(body, merchantDTO.getMerchantNo());
        qrCodeDTO.setBody(format1);
        // 获取二维码的URL
        String storeQRCodeURL = transactionService.createStoreQRCode(qrCodeDTO);
        // 调用工具类生成二维码图片
        QRCodeUtil qrCodeUtil = new QRCodeUtil();
        String qrCodeUtil1 = qrCodeUtil.createQRCode(storeQRCodeURL,200,300);
        return null;
    }
}
