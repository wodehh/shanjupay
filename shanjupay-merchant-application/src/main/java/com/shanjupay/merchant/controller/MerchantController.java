package com.shanjupay.merchant.controller;

import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.CommonErrorCode;
import com.shanjupay.common.util.PhoneUtil;
import com.shanjupay.common.util.StringUtil;
import com.shanjupay.merchant.MerchantService;
import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.common.util.SecurityUtil;
import com.shanjupay.merchant.convert.MerchantDetailConvert;
import com.shanjupay.merchant.convert.MerchantRegisterConvert;
import com.shanjupay.merchant.service.FileService;
import com.shanjupay.merchant.service.SmsService;
import com.shanjupay.merchant.vo.MerchantDetailVO;
import com.shanjupay.merchant.vo.MerchantRegisterVO;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;


/**
 * @author Administrator
 * @version 1.0
 **/
@RestController
@Api(value = "商户平台应用接口", tags = "商户平台应用接口", description = "商户平台应用接口")
public class MerchantController {


    // 注入远程调用的接口
    @org.apache.dubbo.config.annotation.Reference
    MerchantService merchantService;

    // 将本地的bean进行注入
    @Autowired
    SmsService smsService;

    @Autowired
    FileService fileService;

    @ApiOperation(value = "根据id查询商户信息")
    @GetMapping("/merchants/{id}")
    public MerchantDTO queryMerchantById(@PathVariable("id") Long id) {
        MerchantDTO merchantDTO = merchantService.queryMerchantById(id);
        return merchantDTO;
    }


    // 上传证件照
    @ApiOperation(value = "上传证件照")
    @PostMapping("/upload")
    public String upload(@ApiParam(value = "证件照", required = true)
                         @RequestParam("file") MultipartFile multipartFile) throws IOException {
        // 调用fileService上传文件
        // 生成文件名称fileName，要保证它的唯一
        // 文件原始名称

        String originalFilename = multipartFile.getOriginalFilename();
        // 扩展名
        String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") - 1);
        // 文件名称
        String fileName = UUID.randomUUID() + suffix;
        // byte[] bytes,String fileName
        return fileService.upLoad(multipartFile.getBytes(), fileName);
    }


    @ApiOperation("测试")
    @GetMapping(path = "/hello")
    public String hello() {
        return "hello";
    }


    @ApiOperation("测试")
    @ApiImplicitParam(name = "name", value = "姓名", required = true, dataType = "string")
    @PostMapping(value = "/hi")
    public String hi(String name) {
        return "hi," + name;
    }

    @ApiOperation("资质申请")
    @PostMapping("/my/merchants/save")
    @ApiImplicitParams({@ApiImplicitParam(name = "merchantInfo",value = "商户认证资料",
            required = true,dataType = "MerchantDetailVO",paramType = "body")})
    public void saveMerchant(@RequestBody MerchantDetailVO merchantInfo){
        // 解析token取出当前登录商户ID
        Long merchantId = SecurityUtil.getMerchantId();

        MerchantDTO merchantDTO = MerchantDetailConvert.INSTANCE.vo2dto(merchantInfo);
        merchantService.applyMerchant(merchantId,merchantDTO);
    }


    @ApiOperation("获取登录用户的商户信息")
    @GetMapping("/my/merchants")
    public MerchantDTO getMerchantInfo(){
        // 从token中获取商户id
        Long merchantId = SecurityUtil.getMerchantId();
        return merchantService.queryMerchantById(merchantId);
    }


    @ApiOperation("获取手机验证码")
    @ApiImplicitParam(name = "phone", value = "手机号", required = true, dataType = "string", paramType = "query")
    @GetMapping(value = "/sms")
    public String getSmsCode(@RequestParam("phone") String phone) {
        return smsService.sendMsg(phone);
    }


    @ApiOperation("商户注册")
    @ApiImplicitParam(value = "商户注册信息", name = "merchantRegisterVO",
            required = true, dataType = "MerchantRegisterVO", paramType = "body")
    @PostMapping("/merchants/register")
    public MerchantRegisterVO registerMerchant(@RequestBody MerchantRegisterVO merchantRegisterVO) {

        // 校验参数合法性
        if (merchantRegisterVO == null) {
            throw new BusinessException(CommonErrorCode.E_100108);
        }
        // 校验手机号是否为空
        if (StringUtil.isBlank(merchantRegisterVO.getMobile())) {
            throw new BusinessException(CommonErrorCode.E_100112);
        }
        // 校验手机号格式不正确
        if (PhoneUtil.isMatches(merchantRegisterVO.getMobile())) {
            throw new BusinessException(CommonErrorCode.E_100109);
        }
        // 校验验证码
        smsService.checkVerifyCode(merchantRegisterVO.getVerifyKey(), merchantRegisterVO.getVerifyCode());

        /*// 调用dubbo服务接口
        MerchantDTO merchantDTO = new MerchantDTO();
        // 向dto写入商户注册的信息
        merchantDTO.setMobile(merchantRegisterVO.getMobile());
        merchantDTO.setUsername(merchantRegisterVO.getUsername());*/

        MerchantDTO merchantDTO = MerchantRegisterConvert.INSTANCE.vo2dto(merchantRegisterVO);
        merchantService.createMerchant(merchantDTO);
        return merchantRegisterVO;
    }
}
