package com.shanjupay.merchant.common.intercept;

import com.shanjupay.common.domain.BusinessException;
import com.shanjupay.common.domain.CommonErrorCode;
import com.shanjupay.common.domain.ErrorCode;
import com.shanjupay.common.domain.RestErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author ThinkD
 * @Des 全局异常处理器
 * @date 2021-06-28 21:57
 */
@ControllerAdvice
// 与@Exceptionhandler配合使用实现全局异常处理
public class GlobalExceptionHandler {

    // 定义异常日志输出
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    // 捕获Exception异常
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public RestErrorResponse processException(
            HttpServletRequest request,
            HttpServletResponse response,
            Exception e
    ){
        // 解析异常信息
        if (e instanceof BusinessException){
            // 打印出异常日志
            LOGGER.info(e.getMessage(),e);

            // 解析系统自定义异常信息
            BusinessException businessException = (BusinessException) e;
            ErrorCode errorCode = businessException.getErrorCode();

            // 错误代码
            int code = errorCode.getCode();
            // 错误信息
            String desc = errorCode.getDesc();
            return new RestErrorResponse(String.valueOf(code),desc);
        }

        LOGGER.error("系统异常：",e);
        // 其余异常统一定义为999999未知异常
        return new RestErrorResponse(String.valueOf(CommonErrorCode.UNKNOWN.getCode())
                ,CommonErrorCode.UNKNOWN.getDesc());
    }
}
