package com.shanjupay.merchant.convert;

import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.vo.MerchantRegisterVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author ThinkD
 * @Des TODO
 * @date 2021-06-28 16:53
 */
@Mapper
public interface MerchantRegisterConvert {

    MerchantRegisterConvert INSTANCE = Mappers.getMapper(MerchantRegisterConvert.class);

    //将dto转成vo
    MerchantRegisterVO dto2vo(MerchantDTO merchantDTO);

    //将vo转成dto
    MerchantDTO vo2dto(MerchantRegisterVO merchantRegisterVO);

    public static void main(String[] args) {

        MerchantDTO merchantDTO = new MerchantDTO();
        merchantDTO.setUsername("asda");
        MerchantRegisterVO merchantRegisterVO= MerchantRegisterConvert.INSTANCE.dto2vo(merchantDTO);
        System.out.println(merchantRegisterVO);
    }
}
