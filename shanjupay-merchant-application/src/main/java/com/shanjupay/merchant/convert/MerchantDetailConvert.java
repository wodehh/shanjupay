package com.shanjupay.merchant.convert;

import com.shanjupay.merchant.dto.MerchantDTO;
import com.shanjupay.merchant.vo.MerchantDetailVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author ThinkD
 * @Des TODO
 * @date 2021-06-28 16:53
 */
@Mapper
public interface MerchantDetailConvert {

    MerchantDetailConvert INSTANCE = Mappers.getMapper(MerchantDetailConvert.class);

    //将dto转成vo
    MerchantDetailVO dto2vo(MerchantDTO merchantDTO);

    //将vo转成dto
    MerchantDTO vo2dto(MerchantDetailVO merchantDetailVO);

    public static void main(String[] args) {

        MerchantDTO merchantDTO = new MerchantDTO();
        merchantDTO.setUsername("asda");
        MerchantDetailVO merchantDetailVO= MerchantDetailConvert.INSTANCE.dto2vo(merchantDTO);
        System.out.println(merchantDetailVO);
    }
}
