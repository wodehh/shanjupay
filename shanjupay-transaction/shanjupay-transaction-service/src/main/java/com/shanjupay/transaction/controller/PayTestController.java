package com.shanjupay.transaction.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 支付宝接口测试类
 * @Author ThinkD
 * @Des TODO
 * @date 2021-07-16 9:55
 */
@Controller
@Slf4j
public class PayTestController {

    // 应用id
    String APP_ID = "2021000117688627";
    // 应用私钥
    String APP_PRIVATE_KEY = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCmLOSd24lAcYFJoe8tiEJ9COJVqUxIfHCrle4Q/mKQEREqCwDpdycQl9yfyoPQkm6+PDZORQUvFisXq72Ji29gi1lgtwxAmcMhQCXAKY9pw4gPt4S8/RXRFXtHp1z7+VN+VJDMNbfIdAqihMzylfmTEljySf+2MbruptvpKBolvZC47SydhFK0qIcRYpjFJ1yR1keSZq15Q6VM1Xy+fpjLnTIyEO7wUwB8UB+f5kPDhg7kN14ZuqeqveRgAmnnEZYvAIWZ6FR9jMj/CP4IDmLVCEIk7f99Cjxcwr1et/G4dtuTZneFnCN7JXIpJIMVCgcJmvuFR1f/7Huv6ykKKlkBAgMBAAECggEACfMeLSV77lPdfd8oRv2lnX7urlpYqu5ogJ2Vh4bzt9SNxoqqH1B3ElNFbnvdsJtT7igEyGLxkIQvSFeMHbQvkwRE8bmEDkommNMSb5MNdzOx7d77H6W3rLEwlHW854liKmo90TxWKLZp/Jke/FdmfhvXH4q2uN35Z1iuSiOQjTnXf8EowI6XIOSryXgQJuXX5rlbheUlyAqYNso7n+2Ami1NU/8QH4L65slJF9Q5smQ3Feo8mXzWVHIqgPoDhijCdG21rcaTjL811dDwMjvbG8DvWC94wpL9hfh9odcVr7kx4p6tszugcCVfUA8NvoO22kt6prjrfX/ZqvbGOW/DwQKBgQDuN8Q5gMSZ40j3x2qSWTWLPTDemeFCYjvfRZdRZdylfijLBxLauUV9tRM9G851b3WGa4ahOgBewT5rymes2k4fgS8nQweSScNinolGg2gIsYjr6FTHkAOvtI7wLNaCzc+f1A0mE83xuZo/wDoLoYeTecwVg2npMo3qm9D7rl44xQKBgQCylG2McZps6afM/xVoWUlRKcRRRbiWVNpZo+tJvMRcwCbuQnQGN19LVwN7XWKZ90y30620BPNyUhwdThFxGYgvnu64JH8S4e0+YiUIeB7TxUnmNmmpkferIF9oUBrf/UEYAh4PTCQSMeYLsdRtsYXBMjuoBD3Xz4AGEK5S7D0LDQKBgQCtzQQeTCOkfx4yoUzQ38NgBlIMZsrhixhRqs1e9umH2MbcchnAeOIJeD5EHHCCX8KoQvUj1lSEzYrETBVeGJxdAfysMYxpn+WsoyxWxPL3PMhADe8k9p6p6BtNgfkz0AMGgkjhmhtsnFS85Hu1FrH62CcdHxb7rAW8wNg1eB+IXQKBgQCxp0FYH0feI5BAOl9koLZ0XNWleHPKcf2Kgay3fWfnUW0pR8i+ymUtwRGAr4VA4CldEuNSHD7HsvGvIZPubNTzcuvPGtejxEhM8DwhMcaRLEp5EBaLj9/QSPNBBbfGrt4L8UdQNpIH28YcSjunKJyEduGsIxQW1qFo5z1U1xlfHQKBgQDQRfozZO8l2CELOhNGFkdSRn5n0PqYl2BpFgtgauuRv8RFMB6ckKIcrskhkozeBW6ZQgWmgfQV4qG3fflErdIn5wqwwLoJLjWYIV3oK/61IwyShBraSoXurTSoFk7MQzV+W1t/Wfb6pPlv2l/HG3X0lCleqfZCP+Fo8kK8TT/FhQ==";
    // 支付宝公钥
    String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApizknduJQHGBSaHvLYhCfQjiValMSHxwq5XuEP5ikBERKgsA6XcnEJfcn8qD0JJuvjw2TkUFLxYrF6u9iYtvYItZYLcMQJnDIUAlwCmPacOID7eEvP0V0RV7R6dc+/lTflSQzDW3yHQKooTM8pX5kxJY8kn/tjG67qbb6SgaJb2QuO0snYRStKiHEWKYxSdckdZHkmateUOlTNV8vn6Yy50yMhDu8FMAfFAfn+ZDw4YO5DdeGbqnqr3kYAJp5xGWLwCFmehUfYzI/wj+CA5i1QhCJO3/fQo8XMK9XrfxuHbbk2Z3hZwjeyVyKSSDFQoHCZr7hUdX/+x7r+spCipZAQIDAQAB";
    // 编码字符集设置
    String CHARSET = "utf-8";
    // 支付宝接口的网关地址，正式"https://openapi.alipay.com/gateway.do"
    String serverUrl = "https://openapi.alipaydev.com/gateway.do";
    // 签名算法类型
    String sign_type = "RSA2";

    @GetMapping("/alipaytest")
    public void alipaytest(HttpServletRequest httpRequest,
                           HttpServletResponse httpResponse) throws ServletException, IOException {
        // 构造sdk的客户端对象
        AlipayClient alipayClient = new DefaultAlipayClient(serverUrl, APP_ID, APP_PRIVATE_KEY, "json", CHARSET, ALIPAY_PUBLIC_KEY, sign_type); //获得初始化的AlipayClient
        AlipayTradeWapPayRequest alipayRequest = new AlipayTradeWapPayRequest();//创建API对应的request
        // alipayRequest.setReturnUrl("http://domain.com/CallBack/return_url.jsp");
        // alipayRequest.setNotifyUrl("http://domain.com/CallBack/notify_url.jsp");//在公共参数中设置回跳和通知地址
        alipayRequest.setBizContent("{" +
                " \"out_trade_no\":\"20150420010101017\"," +
                " \"total_amount\":\"88.88\"," +
                " \"subject\":\"Iphone6 16G\"," +
                " \"product_code\":\"QUICK_WAP_PAY\"" +
                " }");
        // 填充业务参数
        String form="";
        try {
            // 请求支付宝下单接口,发起http请求
            // 调用SDK生成表单
            form = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        httpResponse.setContentType("text/html;charset=" + CHARSET);
        httpResponse.getWriter().write(form);//直接将完整的表单html输出到页面
        httpResponse.getWriter().flush();
        httpResponse.getWriter().close();
    }
}
